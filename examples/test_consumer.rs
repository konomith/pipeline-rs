use std::path::Path;
use std::str::from_utf8;

use pipeline::config::Config;
use pipeline::consumer::*;
use pipeline::logging;
use pipeline::util::rmq::ConnectionConfig;

#[tokio::main]
async fn main() {
    let path = Path::new("./src/bin/test_config.toml");
    let config = Config::from_path(path).unwrap();

    let _ = logging::simple_config("INFO").unwrap();

    let consumer_config = ConnectionConfig::builder()
        .host(config.get_str("consumer", "host").unwrap())
        .vhost(config.get_str("consumer", "vhost").unwrap())
        .queue(config.get_str("consumer", "queue").unwrap())
        .username(config.get_str("consumer", "username").unwrap())
        .password(config.get_str("consumer", "password").unwrap())
        .auth_required(true)
        .secure(false)
        .build();

    let mut consumer = RMQConsumer::new(consumer_config);

    loop {
        match consumer.next_message().await {
            Ok(message) => {
                println!("Got message: {:?}", from_utf8(&message).unwrap());
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
        }
    }
}
