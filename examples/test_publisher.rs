use std::env;
use std::path::Path;

use pipeline::config::Config;
use pipeline::logging;
use pipeline::message::*;
use pipeline::publisher::*;
use pipeline::util::rmq::ConnectionConfig;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let path = Path::new("./src/bin/test_config.toml");
    let config = Config::from_path(path).unwrap();

    let _ = logging::simple_config("DEBUG").unwrap();

    let publisher_config = ConnectionConfig::builder()
        .host(config.get_str("publisher", "host").unwrap())
        .vhost(config.get_str("publisher", "vhost").unwrap())
        .queue(config.get_str("publisher", "queue").unwrap())
        .username(config.get_str("publisher", "username").unwrap())
        .password(config.get_str("publisher", "password").unwrap())
        .auth_required(true)
        .secure(false)
        .build();

    let mut args = env::args();
    let _ = args.next();

    let message = args.next().unwrap_or("Test message".to_string());
    let response_type = args.next().unwrap_or("console".to_string());

    let response = match &response_type.to_lowercase()[..] {
        "console" => ResponseMetadata::CONSOLE(ConsoleResponseMetadata::new(ConsolePayload {
            header: "cpheader".to_owned(),
            result: "".to_owned(),
        })),
        "tcp" => ResponseMetadata::TCP(TcpResponseMetadata::new(TcpPayload {
            host: "localhost".to_owned(),
            port: 9000,
            result: "".to_owned(),
        })),
        "email" => ResponseMetadata::EMAIL(EmailResponseMetadata::new(EmailPayload {
            email: "foo@localhost".to_owned(),
            subject: "[pipeline] task result".to_owned(),
            payload: "".to_owned(),
        })),
        _ => ResponseMetadata::CONSOLE(ConsoleResponseMetadata::new(ConsolePayload {
            header: "cpheader".to_owned(),
            result: "".to_owned(),
        })),
    };

    let mut publisher = RMQPublisher::new(publisher_config);

    let echo = EchoMessage::new(&message).with_response_metadata(response);
    let serialized = echo.serialize().unwrap();
    let _connect = publisher.connect().await?;
    let _res = publisher.publish(&serialized).await?;
    publisher.close().await;
    Ok(())
}
