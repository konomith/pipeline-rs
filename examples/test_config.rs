use pipeline::config::Config;
use std::path::Path;
use toml;

fn main() {
    let path = Path::new("./src/bin/test_config.toml");
    let cfg = Config::from_path(path).unwrap();
    let tenv = cfg.get_section("envtest").unwrap();
    display(&tenv);
}

fn display(cfg: &toml::Value) {
    let as_tab = cfg.as_table().unwrap();
    for (k, v) in as_tab.iter() {
        if v.is_table() {
            println!("{}", k);
            display(v);
        } else {
            println!("\t{}:{}", k, v.to_string());
        }
    }
}
