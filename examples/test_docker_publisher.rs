use std::env;
use std::path::Path;

use pipeline::config::Config;
use pipeline::logging;
use pipeline::message::{
    ConsolePayload, ConsoleResponseMetadata, DockerImageDetails, DockerImageMessage,
    ResponseMetadata,
};
use pipeline::prelude::*;
use pipeline::publisher::*;
use pipeline::util::rmq::ConnectionConfig;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let path = Path::new("./src/bin/test_config.toml");
    let config = Config::from_path(path).unwrap();

    let _ = logging::simple_config("DEBUG").unwrap();

    let publisher_config = ConnectionConfig::builder()
        .host(config.get_str("publisher", "host").unwrap())
        .vhost(config.get_str("publisher", "vhost").unwrap())
        .queue(config.get_str("publisher", "queue").unwrap())
        .username(config.get_str("publisher", "username").unwrap())
        .password(config.get_str("publisher", "password").unwrap())
        .auth_required(true)
        .secure(false)
        .build();

    let mut args = env::args();
    let _ = args.next();

    let name = args.next().unwrap_or("registry".to_string());
    let tag = args.next().unwrap_or("2".to_string());
    let dest_tag = args.next().unwrap_or(tag.clone());

    let mut publisher = RMQPublisher::new(publisher_config);

    let response = ResponseMetadata::CONSOLE(ConsoleResponseMetadata::new(ConsolePayload {
        header: "cpheader".to_owned(),
        result: "".to_owned(),
    }));

    let source = DockerImageDetails::new("0.0.0.0:4000", &name, &tag, None, None);
    let dest = DockerImageDetails::new("0.0.0.0:6000", &name, &dest_tag, None, None);
    let docker = DockerImageMessage::new(source, dest, Some(response));

    let serialized = docker.serialize().unwrap();
    let _connect = publisher.connect().await?;
    let _res = publisher.publish(&serialized).await?;
    publisher.close().await;
    Ok(())
}
