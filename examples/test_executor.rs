use std::env;
use std::path::Path;

use tokio::signal::unix::{signal, SignalKind};

use pipeline::config::Config;
use pipeline::consumer::*;
use pipeline::executor::*;
use pipeline::handler::{DockerImageHandler, EchoHandler, ResponseHandler};
use pipeline::logging;
use pipeline::publisher::*;
use pipeline::types::ActionType;
use pipeline::util::rmq::ConnectionConfig;

async fn stop(exec: &Executor) -> ExecutorResult {
    let mut sigint_stream = signal(SignalKind::interrupt())?;
    let mut sigterm_stream = signal(SignalKind::terminate())?;
    tokio::select! {
        _sigint = sigint_stream.recv() => {
            log::info!("Caught SIGINT");
            exec.stop().await;
            Ok(())
        }
        _sigterm = sigterm_stream.recv() => {
            log::info!("Caught SIGTERM");
            exec.stop().await;
            Ok(())
        }
    }
}

#[tokio::main]
async fn main() {
    let mut args = env::args();
    let _ = args.next();
    let cfg_path = args
        .next()
        .unwrap_or("./src/bin/test_config.toml".to_string());
    let path = Path::new(&cfg_path);
    // let path = Path::new("./src/bin/test_config.toml");
    let config = Config::from_path(path).unwrap();

    let _ = logging::simple_config("INFO").unwrap();

    let consumer_config = ConnectionConfig::builder()
        .host(config.get_str("consumer", "host").unwrap())
        .vhost(config.get_str("consumer", "vhost").unwrap())
        .queue(config.get_str("consumer", "queue").unwrap())
        .username(config.get_str("consumer", "username").unwrap())
        .password(config.get_str("consumer", "password").unwrap())
        .auth_required(true)
        .secure(false)
        .build();

    let consumer = RMQConsumer::new(consumer_config);
    let mut executor = Executor::new(&config).register_consumer(consumer);
    executor.register_handler(ActionType::Echo, EchoHandler);
    executor.register_handler(ActionType::Response, ResponseHandler::new(&config));

    if !config
        .get_bool("general", "upstream_sentinel")
        .unwrap_or(false)
    {
        let publisher_config = ConnectionConfig::builder()
            .host(config.get_str("publisher", "host").unwrap())
            .vhost(config.get_str("publisher", "vhost").unwrap())
            .queue(config.get_str("publisher", "queue").unwrap())
            .username(config.get_str("publisher", "username").unwrap())
            .password(config.get_str("publisher", "password").unwrap())
            .auth_required(true)
            .secure(false)
            .build();

        let publisher = RMQPublisher::new(publisher_config);
        executor = executor.register_upstream_publisher(publisher);
    }

    if !config
        .get_bool("general", "downstream_sentinel")
        .unwrap_or(false)
    {
        let publisher_config = ConnectionConfig::builder()
            .host(config.get_str("publisher", "host").unwrap())
            .vhost(config.get_str("publisher", "vhost").unwrap())
            .queue(config.get_str("publisher", "queue").unwrap())
            .username(config.get_str("publisher", "username").unwrap())
            .password(config.get_str("publisher", "password").unwrap())
            .auth_required(true)
            .secure(false)
            .build();

        let publisher = RMQPublisher::new(publisher_config);
        executor = executor.register_downstream_publisher(publisher);
    }

    let docker_handler = DockerImageHandler::new(&config).expect("Unable to create docker handler");
    executor.register_handler(ActionType::DockerImage, docker_handler);

    let run = executor.run();
    let stopper = stop(&executor);
    println!("{:?}", tokio::try_join!(run, stopper));
}
