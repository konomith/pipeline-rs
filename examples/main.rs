// use pipeline::config::Config;
// use pipeline::handler::*;
// use pipeline::message::{
//     BaseMessage, DockerImageDetails, DockerImageMessage, EchoMessage, EmailPayload,
//     EmailResponseMetadata, MessageError, MessageErrorKind, ResponseMetadata, WebhookPayload,
//     WebhookResponseMetadata,
// };
// use pipeline::prelude::*;
// use pipeline::types::*;

// use std::collections::HashMap;
// use std::path::Path;
// use std::str::from_utf8;

// use async_trait::async_trait;
// use serde::{Deserialize, Serialize};
// use serde_json;

// #[tokio::main]
// async fn main() {
//     let whp = WebhookPayload {
//         address: String::from("http://localhost"),
//         payload: String::from("This is the payload"),
//         headers: HashMap::new(),
//         auth_required: false,
//     };

//     let md = WebhookResponseMetadata::new(whp);

//     let emp = EmailPayload {
//         email: "foo@bar.com".to_string(),
//         subject: "Response subject".to_string(),
//         payload: "This is the email body with some information".to_string(),
//     };

//     let ed = EmailResponseMetadata::new(emp);

//     let mut messages: Vec<Box<dyn Message>> = vec![
//         Box::new(EchoMessage::new("this is a message")),
//         Box::new(DockerImageMessage::new(
//             DockerImageDetails::new("foo", "bar", "baz", "username", "password"),
//             DockerImageDetails::new("qux", "name", "tag", "uname", "pass"),
//             Some(ResponseMetadata::WEBHOOK(md)),
//         )),
//         Box::new(DockerImageMessage::new(
//             DockerImageDetails::new("foo1", "bar1", "baz1", "username1", "password1"),
//             DockerImageDetails::new("qux", "name1", "tag1", "uname1", "pass1"),
//             Some(ResponseMetadata::EMAIL(ed)),
//         )),
//     ];
//     let mut map: HashMap<ActionType, fn(&[u8])> = HashMap::new();

//     map.insert(ActionType::Echo, handle_test);
//     map.insert(ActionType::DockerImage, handle_docker);

//     let at = ActionType::CustomAction("custom".to_string());
//     let custom_msg = CustomMessage::new(at.clone(), "This is custom");

//     messages.push(Box::new(custom_msg));
//     map.insert(at.clone(), handle_custom);

//     for m in &messages {
//         let serialized = m.serialize().unwrap();
//         let deserialized = <BaseMessage as Message>::deserialize(&serialized).unwrap();
//         let action = deserialized.action();
//         let handler = map.get(&action).unwrap();
//         handler(&serialized);
//     }

//     let path = Path::new("./src/bin/test_config.toml");

//     let config = Config::from_path(path).unwrap();
//     println!("{}", config.get_bool("executor", "upstream_sentinel").unwrap());

//     let mut handlers: HashMap<ActionType, Box<dyn MessageHandler>> = HashMap::new();
//     handlers.insert(ActionType::Echo, Box::new(EchoHandler));
//     handlers.insert(ActionType::DockerImage, Box::new(DockerImageHandler));
//     handlers.insert(at.clone(), Box::new(CustomHandler::new(2)));

//     for m in &messages {
//         let serialized = m.serialize().unwrap();

//         println!("serialized={}", from_utf8(&serialized).unwrap());
//         let deserialized = <BaseMessage as Message>::deserialize(&serialized).unwrap();
//         let action = deserialized.action();

//         let payload = deserialized.payload();
//         println!("{:?}", payload);

//         match handlers.get(&action) {
//             Some(handler) => {
//                 handler.handle(&serialized, &config).await;
//             }
//             None => {
//                 println!("No handler found for {:?}", action);
//             }
//         }
//     }
// }

// #[derive(Debug, Serialize, Deserialize)]
// struct CustomMessage {
//     #[serde(rename(serialize = "action", deserialize = "action"))]
//     t: ActionType,
//     m: String,
// }

// impl CustomMessage {
//     fn new(t: ActionType, m: &str) -> CustomMessage {
//         CustomMessage {
//             t,
//             m: m.to_string(),
//         }
//     }
// }

// impl<'a> Message<'a> for CustomMessage {
//     fn serialize(&self) -> Result<Vec<u8>, MessageError> {
//         match serde_json::to_vec(&self) {
//             Ok(result) => Ok(result),
//             Err(e) => {
//                 let err = MessageError::new(
//                     MessageErrorKind::SerializationError,
//                     Some(e.to_string()),
//                     Some(Box::new(e)),
//                 );
//                 Err(err)
//             }
//         }
//     }

//     fn deserialize(raw: &'a [u8]) -> Result<Self, MessageError> {
//         match serde_json::from_slice(raw) {
//             Ok(result) => Ok(result),
//             Err(e) => Err(MessageError::new(
//                 MessageErrorKind::DeserializationError,
//                 Some(e.to_string()),
//                 Some(Box::new(e)),
//             )),
//         }
//     }

//     fn action(&self) -> ActionType {
//         self.t.clone()
//     }
// }

// #[derive(Debug)]
// struct CustomHandler {
//     version: i32,
// }

// impl CustomHandler {
//     fn new(v: i32) -> CustomHandler {
//         CustomHandler { version: v }
//     }
// }

// #[async_trait]
// impl MessageHandler for CustomHandler {
//     async fn handle(&self, raw: &[u8], config: &Config) -> HandleResult {
//         let message = <CustomMessage as Message>::deserialize(raw).unwrap();
//         println!(
//             "[{}] CustomHandler says: {}",
//             config.get_str("executor", "name").unwrap(),
//             message.m
//         );
//         HandleResult {
//             success: true,
//             forward: None,
//             reply: None,
//         }
//     }
// }

// fn handle_test(payload: &[u8]) {
//     let m = EchoMessage::deserialize(payload).unwrap();
//     println!("{:?}", m);
// }

// fn handle_docker(payload: &[u8]) {
//     let m = DockerImageMessage::deserialize(payload).unwrap();
//     println!("{:?}", m);
// }

// fn handle_custom(payload: &[u8]) {
//     let m = <CustomMessage as Message>::deserialize(payload).unwrap();
//     println!("{:?}", m);
// }
fn main() {
    unimplemented!();
}
