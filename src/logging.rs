use log4rs::append::console::{ConsoleAppender, Target};
use log4rs::config::{Appender, Config, Logger, Root};
use log4rs::encode::pattern::PatternEncoder;

const LOG_FORMAT: &str = "{d(%Y-%m-%d %H:%M:%S%.3f%z)(utc)} [{l}] [{f}|{M}|{L}]: {m}\n";

pub fn simple_config(level: &str) -> Result<log4rs::Handle, log::SetLoggerError> {
    let level: log::Level = level.parse().unwrap_or_else(|_e| log::Level::Info);

    let stdout = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(LOG_FORMAT)))
        .build();
    let stderr = ConsoleAppender::builder()
        .target(Target::Stderr)
        .encoder(Box::new(PatternEncoder::new(LOG_FORMAT)))
        .build();

    let config = Config::builder()
        .appender(Appender::builder().build("stderr", Box::new(stderr)))
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .logger(Logger::builder().build("exporter", level.to_level_filter()))
        .build(
            Root::builder()
                .appender("stderr")
                .build(level.to_level_filter()),
        )
        .unwrap();

    log4rs::init_config(config)
}
