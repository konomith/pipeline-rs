use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct ConsumerError {
    kind: ConsumerErrorKind,
    detail: Option<String>,
    source: Option<Box<dyn Error + Send + Sync>>,
}

#[derive(Debug, Clone)]
pub enum ConsumerErrorKind {
    GenericError,
    ConnectionError,
    InitError,
    CommunicationError,
}

impl Error for ConsumerError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.source {
            Some(s) => s.source(),
            None => None,
        }
    }
}

impl fmt::Display for ConsumerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let detail = match &self.detail {
            Some(d) => d.to_owned(),
            None => "Unknown".to_string(),
        };

        write!(f, "{}: {}", self.kind.as_str(), &detail)
    }
}

impl ConsumerError {
    pub fn new(
        kind: ConsumerErrorKind,
        detail: Option<String>,
        source: Option<Box<dyn Error + Send + Sync>>,
    ) -> ConsumerError {
        ConsumerError {
            kind,
            detail,
            source,
        }
    }

    pub fn kind(&self) -> ConsumerErrorKind {
        self.kind.clone()
    }
}

impl From<ConsumerErrorKind> for ConsumerError {
    fn from(kind: ConsumerErrorKind) -> Self {
        let detail = String::from(kind.as_str());
        ConsumerError::new(kind, Some(detail), None)
    }
}

impl ConsumerErrorKind {
    pub(crate) fn as_str(&self) -> &'static str {
        match *self {
            ConsumerErrorKind::GenericError => "Failed to consume",
            ConsumerErrorKind::ConnectionError => "Consumer connection failed",
            ConsumerErrorKind::InitError => "Failed to initialize properly",
            ConsumerErrorKind::CommunicationError => "Communication Failed",
        }
    }
}
