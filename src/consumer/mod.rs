use core::fmt::Debug;

use async_trait::async_trait;

mod error;
mod rmq;
pub type ConsumerResult = Result<Vec<u8>, error::ConsumerError>;

#[async_trait]
pub trait Consume: Debug + Send + Sync {
    async fn connect(&mut self) -> Result<(), error::ConsumerError>;
    async fn next_message(&mut self) -> ConsumerResult;
    async fn close(&mut self);
}

pub use error::*;
pub use rmq::*;
