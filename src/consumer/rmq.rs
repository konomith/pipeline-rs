use std::sync::Arc;

use async_trait::async_trait;
use backoff::{future::retry, ExponentialBackoff};
use futures_lite::StreamExt;
use lapin::{options::*, types::FieldTable, Channel, Connection, ConnectionProperties, Consumer};
use log;
use tokio::sync::Mutex;

use super::{Consume, ConsumerError, ConsumerErrorKind, ConsumerResult};
use crate::util::rmq::{generate_ctag, ConnectionConfig};

#[derive(Debug, Clone)]
pub struct RMQConsumer {
    inner: Arc<Mutex<RMQConsumerInner>>,
}

#[async_trait]
impl Consume for RMQConsumer {
    async fn connect(&mut self) -> Result<(), ConsumerError> {
        let mut inner = self.inner.lock().await;
        inner.connect().await
    }

    async fn next_message(&mut self) -> ConsumerResult {
        retry(ExponentialBackoff::default(), || async {
            let mut inner = self.inner.lock().await;
            Ok(inner.next_message().await?)
        })
        .await
    }

    async fn close(&mut self) {
        let mut inner = self.inner.lock().await;
        inner.reset_members().await;
    }
}

impl RMQConsumer {
    pub fn new(config: ConnectionConfig) -> RMQConsumer {
        let inner = Arc::new(Mutex::new(RMQConsumerInner {
            connection: None,
            channel: None,
            consumer: None,
            config,
        }));

        RMQConsumer { inner }
    }
}

#[derive(Debug)]
struct RMQConsumerInner {
    connection: Option<Connection>,
    channel: Option<Channel>,
    consumer: Option<Consumer>,
    config: ConnectionConfig,
}

impl RMQConsumerInner {
    async fn connect(&mut self) -> Result<(), ConsumerError> {
        let conn = self._connect().await?;
        let chan = self._channel(&conn).await?;
        let _ = self._declare_queue(&chan).await?;
        let consumer = self._consumer(&chan).await?;

        self.connection = Some(conn);
        self.channel = Some(chan);
        self.consumer = Some(consumer);

        Ok(())
    }

    async fn _connect(&self) -> Result<Connection, ConsumerError> {
        let uri = self.config.generate_uri();
        match Connection::connect(&uri, ConnectionProperties::default()).await {
            Ok(c) => Ok(c),
            Err(e) => {
                let err = ConsumerError::new(
                    ConsumerErrorKind::ConnectionError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    async fn _channel(&self, conn: &Connection) -> Result<Channel, ConsumerError> {
        match conn.create_channel().await {
            Ok(chan) => {
                let _ = chan.basic_qos(1, BasicQosOptions::default()).await;
                Ok(chan)
            }
            Err(e) => {
                let err = ConsumerError::new(
                    ConsumerErrorKind::InitError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    async fn _declare_queue(&self, chan: &Channel) -> Result<(), ConsumerError> {
        let mut queue_options = QueueDeclareOptions::default();
        queue_options.durable = true;
        queue_options.auto_delete = false;

        match chan
            .queue_declare(&self.config.queue, queue_options, FieldTable::default())
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                let err = ConsumerError::new(
                    ConsumerErrorKind::InitError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    async fn _consumer(&self, chan: &Channel) -> Result<Consumer, ConsumerError> {
        let ctag = generate_ctag(chan);
        match chan
            .basic_consume(
                &self.config.queue,
                &ctag,
                BasicConsumeOptions::default(),
                FieldTable::default(),
            )
            .await
        {
            Ok(consumer) => Ok(consumer),
            Err(e) => {
                let err = ConsumerError::new(
                    ConsumerErrorKind::InitError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    async fn next_message(&mut self) -> ConsumerResult {
        if self.connection.is_none() || self.channel.is_none() || self.consumer.is_none() {
            let _ = self.connect().await?;
        }

        if let Some(ref mut consumer) = self.consumer {
            match consumer.next().await {
                Some(delivery) => match delivery {
                    Ok(delivery) => {
                        let payload = delivery.data.clone();
                        let _ = delivery.ack(BasicAckOptions::default()).await;
                        Ok(payload)
                    }
                    Err(e) => {
                        self.reset_members().await;
                        let err = ConsumerError::new(
                            ConsumerErrorKind::CommunicationError,
                            Some(e.to_string()),
                            Some(Box::new(e)),
                        );
                        log::warn!("{}", err);
                        Err(err)
                    }
                },
                None => {
                    self.reset_members().await;
                    let err = ConsumerError::new(
                        ConsumerErrorKind::ConnectionError,
                        Some("Consumer got canceled".to_string()),
                        None,
                    );
                    log::warn!("{}", err);
                    Err(err)
                }
            }
        } else {
            self.reset_members().await;
            let err = ConsumerError::new(
                ConsumerErrorKind::ConnectionError,
                Some("No available consumer".to_string()),
                None,
            );
            log::warn!("{}", err);
            Err(err)
        }
    }

    async fn reset_members(&mut self) {
        if let Some(consumer) = self.consumer.take() {
            drop(consumer);
        }
        if let Some(chan) = &self.channel {
            let _ = chan.close(200, "Close channel").await;
            self.channel = None;
        }
        if let Some(conn) = &self.connection {
            let _ = conn.close(200, "Close connection").await;
            self.connection = None;
        }
    }
}
