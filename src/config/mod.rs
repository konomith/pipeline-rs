use std::env;
use std::error::Error as StdError;
use std::fs;
use std::ops::Deref;
use std::path::Path;
use std::sync::Arc;

use regex;
use toml;

lazy_static! {
    static ref ENV_REGEX: regex::Regex = regex::Regex::new(r"^\$\{ENV_(?P<var>\w*)\}$").unwrap();
}

#[derive(Debug)]
pub struct ConfigRef {
    store: toml::Value,
}

impl ConfigRef {
    pub fn get(&self, section: &str, key: &str) -> Option<&toml::Value> {
        self.store.get(section)?.get(key)
    }

    pub fn get_section(&self, section: &str) -> Option<&toml::Value> {
        self.store.get(section)
    }

    pub fn get_int(&self, section: &str, key: &str) -> Option<i64> {
        self.get(section, key)?.as_integer()
    }

    pub fn get_float(&self, section: &str, key: &str) -> Option<f64> {
        self.get(section, key)?.as_float()
    }

    pub fn get_str(&self, section: &str, key: &str) -> Option<&str> {
        self.get(section, key)?.as_str()
    }

    pub fn get_bool(&self, section: &str, key: &str) -> Option<bool> {
        self.get(section, key)?.as_bool()
    }
}

#[derive(Debug, Clone)]
pub struct Config {
    inner: Arc<ConfigRef>,
}

impl Config {
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Config, Box<dyn StdError>> {
        let content = fs::read_to_string(path)?;
        let mut store = content.parse::<toml::Value>()?;

        replace_from_env(&mut store);

        Ok(Config {
            inner: Arc::new(ConfigRef { store }),
        })
    }

    pub fn from_str(raw: &str) -> Result<Config, Box<dyn StdError>> {
        let store = toml::from_str(raw)?;
        Ok(Config {
            inner: Arc::new(ConfigRef { store }),
        })
    }
}

impl Deref for Config {
    type Target = ConfigRef;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

fn get_env_key(entry: &str) -> Option<String> {
    if let Some(captures) = ENV_REGEX.captures(entry) {
        if let Some(cap) = captures.name("var") {
            Some(cap.as_str().to_owned())
        } else {
            None
        }
    } else {
        None
    }
}

fn get_env_val(entry: &str) -> String {
    env::var(entry).unwrap_or("".to_owned())
}

fn replace_from_env(store: &mut toml::Value) {
    if let Some(as_table) = store.as_table_mut() {
        for (_, val) in as_table.iter_mut() {
            if val.is_table() {
                replace_from_env(val);
            } else if val.is_str() {
                let v = val.as_str().unwrap();
                if let Some(env_key) = get_env_key(v) {
                    let env_val = get_env_val(&env_key);
                    *val = toml::Value::String(env_val);
                }
            }
        }
    }
}
