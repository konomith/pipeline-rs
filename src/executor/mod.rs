use std::collections::HashMap;
use std::sync::Arc;

use log;
use tokio::sync::{
    mpsc::{unbounded_channel, UnboundedReceiver},
    Mutex, Notify,
};

use crate::config::Config;
use crate::consumer::Consume;
use crate::handler::{HandleResult, MessageHandler};
use crate::message::BaseMessage;
use crate::prelude::*;
use crate::publisher::Publish;
use crate::types::ActionType;

mod error;
mod task;
pub use error::*;
use task::Task;

pub type ExecutorResult = Result<(), ExecutorError>;

#[derive(Debug)]
#[allow(missing_debug_implementations)]
pub struct Executor {
    config: Config,
    consumer: Option<Arc<Mutex<Box<dyn Consume>>>>,
    upstream_publisher: Option<Arc<Mutex<Box<dyn Publish>>>>,
    downstream_publisher: Option<Arc<Mutex<Box<dyn Publish>>>>,
    handlers: HashMap<ActionType, Box<dyn MessageHandler>>,
    consumer_exit: Option<Arc<Notify>>,
}

unsafe impl Sync for Executor {}
unsafe impl Send for Executor {}

impl Executor {
    pub fn new(config: &Config) -> Self {
        let cfg = config.to_owned();
        Executor {
            config: cfg,
            consumer: None,
            upstream_publisher: None,
            downstream_publisher: None,
            handlers: HashMap::new(),
            consumer_exit: None,
        }
    }

    pub fn register_consumer(mut self, consumer: impl Consume + 'static) -> Self {
        self.consumer = Some(Arc::new(Mutex::new(Box::new(consumer))));
        self.consumer_exit = Some(Arc::new(Notify::new()));
        self
    }

    pub fn register_upstream_publisher(mut self, publisher: impl Publish + 'static) -> Self {
        self.upstream_publisher = Some(Arc::new(Mutex::new(Box::new(publisher))));
        self
    }

    pub fn register_downstream_publisher(mut self, publisher: impl Publish + 'static) -> Self {
        self.downstream_publisher = Some(Arc::new(Mutex::new(Box::new(publisher))));
        self
    }

    pub fn register_handler(
        &mut self,
        action: ActionType,
        handler: impl MessageHandler + 'static,
    ) -> Option<Box<dyn MessageHandler>> {
        let old_handler = self.handlers.insert(action, Box::new(handler));
        old_handler
    }

    fn get_cfg_bool(&self, section: &str, key: &str) -> Result<bool, ExecutorError> {
        let val = self
            .config
            .get_bool(section, key)
            .ok_or(ExecutorError::new(
                ExecutorErrorKind::ConfigurationError,
                Some(format!("'{}' missing in section '{}'", key, section)),
                None,
            ))?;
        Ok(val)
    }

    async fn start_consumer(&self) -> Result<UnboundedReceiver<Task>, ExecutorError> {
        let is_up_sentinel = self.get_cfg_bool("executor", "upstream_sentinel")?;
        if !is_up_sentinel && self.upstream_publisher.is_none() {
            return Err(ExecutorError::new(
                ExecutorErrorKind::ConfigurationError,
                Some(String::from(error::NO_UPSTREAM_PUBLISHER)),
                None,
            ));
        }

        let consumer = self
            .consumer
            .as_ref()
            .ok_or(ExecutorError::new(
                ExecutorErrorKind::ConfigurationError,
                Some(String::from(error::NO_CONSUMER)),
                None,
            ))?
            .clone();

        // try to connect first before spawning the consume loop
        let mut connector = consumer.lock().await;
        let _connect = connector.connect().await?;
        drop(connector);

        let (sender, receiver) = unbounded_channel::<Task>();

        let notifier = self
            .consumer_exit
            .as_ref()
            .expect("No exit notifier was registered: This is a bug")
            .clone();

        let sclone = sender.clone();
        let _ = tokio::spawn(async move {
            let mut consumer = consumer.lock().await;
            loop {
                let message = tokio::select! {
                    msg = consumer.next_message() => {
                        log::debug!("msg is {:?}", msg);
                        match msg {
                            Ok(m) => Some(Task::Message(m.to_vec())),
                            Err(e) => Some(Task::Error(ExecutorError::from(e)))
                        }
                    },
                    _ = notifier.notified() => {
                        log::debug!("Received exit notify");
                        None
                    }
                };
                match message {
                    Some(msg) => {
                        let _ = sclone.send(msg);
                    }
                    None => {
                        consumer.close().await;
                        let _ = sclone.send(Task::Close);
                        break;
                    }
                }
            }
        });

        Ok(receiver)
    }

    async fn handle_forward(&self, forward: &[u8]) -> ExecutorResult {
        let is_up_sentinel = self.get_cfg_bool("executor", "upstream_sentinel")?;
        if is_up_sentinel {
            log::warn!("Upstream sentinel, unable to forward message further.");
            Ok(())
        } else if self.upstream_publisher.is_none() {
            Err(ExecutorError::new(
                ExecutorErrorKind::ConfigurationError,
                Some(String::from(error::NO_UPSTREAM_PUBLISHER)),
                None,
            ))
        } else {
            let mut publisher = self.upstream_publisher.as_ref().unwrap().lock().await;
            let _conn = publisher.connect().await;
            publisher.publish(forward).await?;
            Ok(())
        }
    }

    async fn handle_reply(&self, reply: &[u8]) -> ExecutorResult {
        let is_down_sentinel = self.get_cfg_bool("executor", "downstream_sentinel")?;
        if is_down_sentinel {
            log::warn!("Downstream sentinel, unable to relay the reply message any further.");
            Ok(())
        } else if self.downstream_publisher.is_none() {
            Err(ExecutorError::new(
                ExecutorErrorKind::ConfigurationError,
                Some(String::from(error::NO_DOWNSTREAM_PUBLISHER)),
                None,
            ))
        } else {
            let mut publisher = self.downstream_publisher.as_ref().unwrap().lock().await;
            let _conn = publisher.connect().await;
            publisher.publish(reply).await?;
            Ok(())
        }
    }

    async fn handle_result(&self, result: HandleResult) -> Vec<Option<ExecutorError>> {
        log::info!("task handle success: {}", result.success);

        let mut forward_error = None;
        let mut reply_error = None;

        if let Some(forward_result) = result.forward {
            let forward = self.handle_forward(&forward_result).await;
            if let Some(e) = forward.err() {
                forward_error = Some(e);
            }
        }

        if let Some(reply_result) = result.reply {
            let reply = self.handle_reply(&reply_result).await;
            if let Some(e) = reply.err() {
                reply_error = Some(e);
            }
        }

        vec![forward_error, reply_error]
    }

    async fn handle_task(&self, task: Task) -> bool {
        let should_continue = match task {
            Task::Message(msg) => {
                match <BaseMessage as Message>::deserialize(&msg) {
                    Ok(deserialized) => {
                        log::debug!("Got message: {:?}", deserialized);

                        let action = deserialized.action();
                        match self.handlers.get(&action) {
                            Some(handler) => {
                                let res = handler.handle(&msg, &self.config).await;
                                log::debug!("res = {:?}", res);

                                let handle = self.handle_result(res).await;
                                for err in &handle {
                                    if let Some(err) = err {
                                        log::warn!("Error while dispatching: {}", err);
                                    }
                                }
                            }
                            None => {
                                log::warn!("No handler");
                            }
                        }
                    }
                    Err(e) => {
                        log::warn!("Couldn't deserialize: {}", e);
                    }
                }
                true
            }
            Task::Error(e) => {
                log::error!("Received error: {}", e);
                false
            }
            Task::Close => {
                log::info!("Consumer closed");
                false
            }
        };

        should_continue
    }

    pub async fn run(&self) -> ExecutorResult {
        if self.consumer.is_some() {
            let mut rcv: UnboundedReceiver<Task> = self.start_consumer().await?;
            loop {
                match rcv.recv().await {
                    Some(task) => {
                        let cont = self.handle_task(task).await;
                        if !cont {
                            break;
                        }
                    }
                    None => {
                        break;
                    }
                }
            }
        }
        Ok(())
    }

    pub async fn stop(&self) {
        self.consumer_exit
            .as_ref()
            .expect("No exit notifier was registered: This is a bug")
            .notify_one();
    }
}
