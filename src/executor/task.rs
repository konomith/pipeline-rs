use super::error::ExecutorError;

#[derive(Debug)]
pub(crate) enum Task {
    Message(Vec<u8>),
    Error(ExecutorError),
    Close,
}
