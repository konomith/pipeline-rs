use std::error::Error;
use std::fmt;
use std::io::Error as StdIoError;

use crate::consumer::ConsumerError;
use crate::publisher::PublisherError;

#[derive(Debug)]
pub struct ExecutorError {
    kind: ExecutorErrorKind,
    detail: Option<String>,
    source: Option<Box<dyn Error + Send + Sync>>,
}

#[derive(Debug, Clone)]
pub enum ExecutorErrorKind {
    ConfigurationError,
    ConsumeError,
    IoError,
    PublishError,
}

impl Error for ExecutorError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.source {
            Some(s) => s.source(),
            None => None,
        }
    }
}

impl fmt::Display for ExecutorError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let detail = match &self.detail {
            Some(d) => d.to_owned(),
            None => "Unknown".to_string(),
        };

        write!(f, "{}: {}", self.kind.as_str(), &detail)
    }
}

impl ExecutorError {
    pub fn new(
        kind: ExecutorErrorKind,
        detail: Option<String>,
        source: Option<Box<dyn Error + Send + Sync>>,
    ) -> ExecutorError {
        ExecutorError {
            kind,
            detail,
            source,
        }
    }

    pub fn kind(&self) -> ExecutorErrorKind {
        self.kind.clone()
    }
}

impl From<ExecutorErrorKind> for ExecutorError {
    fn from(kind: ExecutorErrorKind) -> Self {
        let detail = String::from(kind.as_str());
        ExecutorError::new(kind, Some(detail), None)
    }
}

impl From<ConsumerError> for ExecutorError {
    fn from(e: ConsumerError) -> Self {
        let detail = e.to_string();
        ExecutorError::new(
            ExecutorErrorKind::ConsumeError,
            Some(detail),
            Some(Box::new(e)),
        )
    }
}

impl From<StdIoError> for ExecutorError {
    fn from(e: StdIoError) -> Self {
        ExecutorError::new(ExecutorErrorKind::IoError, None, Some(Box::new(e)))
    }
}

impl From<PublisherError> for ExecutorError {
    fn from(e: PublisherError) -> Self {
        let detail = e.to_string();
        ExecutorError::new(
            ExecutorErrorKind::PublishError,
            Some(detail),
            Some(Box::new(e)),
        )
    }
}

impl ExecutorErrorKind {
    pub(crate) fn as_str(&self) -> &'static str {
        match *self {
            ExecutorErrorKind::ConfigurationError => "Invalid configuration",
            ExecutorErrorKind::ConsumeError => "Consumer error",
            ExecutorErrorKind::IoError => "IO Error",
            ExecutorErrorKind::PublishError => "PublishError",
        }
    }
}

pub(crate) const NO_UPSTREAM_PUBLISHER: &str = r#"
No upstream publisher is registered even though the executor is not an
upstream sentinel.
Register the publisher with `register_upstream_publisher(...)`
"#;

pub(crate) const NO_CONSUMER: &str = r#"
No consumer was registered. Nothing to do.
"#;

pub(crate) const NO_DOWNSTREAM_PUBLISHER: &str = r#"
No downstream publisher is registered even though the executor is not an
downstream sentinel.
Register the publisher with `register_downstream_publisher(...)`
"#;
