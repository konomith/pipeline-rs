use std::sync::Arc;

use async_trait::async_trait;
use lapin::{
    options::*, publisher_confirm::*, types::FieldTable, BasicProperties, Channel, Connection,
    ConnectionProperties, Error as LapinError,
};
use log;
use tokio::sync::Mutex;

use super::{Publish, PublisherError, PublisherErrorKind};
use crate::util::rmq::ConnectionConfig;

#[derive(Debug, Clone)]
pub struct RMQPublisher {
    inner: Arc<Mutex<RMQPublisherInner>>,
}

impl RMQPublisher {
    pub fn new(config: ConnectionConfig) -> RMQPublisher {
        RMQPublisher {
            inner: Arc::new(Mutex::new(RMQPublisherInner::new(config))),
        }
    }
}

#[async_trait]
impl Publish for RMQPublisher {
    async fn connect(&mut self) -> Result<(), PublisherError> {
        let mut inner = self.inner.lock().await;
        inner.connect().await
    }
    async fn publish(&mut self, payload: &[u8]) -> Result<(), PublisherError> {
        let mut inner = self.inner.lock().await;
        inner.publish(payload).await
    }

    async fn close(&mut self) {
        let mut inner = self.inner.lock().await;
        inner.reset_members().await;
    }
}

#[derive(Debug)]
struct RMQPublisherInner {
    connection: Option<Connection>,
    channel: Option<Channel>,
    config: ConnectionConfig,
}

impl RMQPublisherInner {
    fn new(config: ConnectionConfig) -> RMQPublisherInner {
        RMQPublisherInner {
            connection: None,
            channel: None,
            config,
        }
    }

    async fn connect(&mut self) -> Result<(), PublisherError> {
        let conn = self._connect().await?;
        let chan = self._channel(&conn).await?;
        let _ = self._declare_queue(&chan).await?;

        self.connection = Some(conn);
        self.channel = Some(chan);

        Ok(())
    }

    async fn _connect(&self) -> Result<Connection, PublisherError> {
        let uri = self.config.generate_uri();
        match Connection::connect(&uri, ConnectionProperties::default()).await {
            Ok(c) => Ok(c),
            Err(e) => {
                let err = PublisherError::new(
                    PublisherErrorKind::ConnectionError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    async fn _channel(&self, conn: &Connection) -> Result<Channel, PublisherError> {
        match conn.create_channel().await {
            Ok(chan) => {
                // make confirmation required
                let _ = chan.confirm_select(ConfirmSelectOptions::default()).await;
                Ok(chan)
            }
            Err(e) => {
                let err = PublisherError::new(
                    PublisherErrorKind::ConnectionError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    async fn _declare_queue(&self, chan: &Channel) -> Result<(), PublisherError> {
        let mut queue_options = QueueDeclareOptions::default();
        queue_options.durable = true;
        queue_options.auto_delete = false;

        match chan
            .queue_declare(&self.config.queue, queue_options, FieldTable::default())
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                log::debug!("queue declare error: {}", e);
                let err = PublisherError::new(
                    PublisherErrorKind::ConnectionError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    async fn handle_confirm(
        &self,
        confirm: Result<PublisherConfirm, LapinError>,
    ) -> Result<(), PublisherError> {
        log::debug!("Confirm: {:?}", confirm);

        let nacked_error = Err(PublisherError::new(
            PublisherErrorKind::CommunicationError,
            Some("message was not acked".to_string()),
            None,
        ));
        match confirm {
            Ok(publisher_confirm) => match publisher_confirm.await {
                Ok(confirmation) => {
                    log::debug!("confirmation = {:?}", confirmation);
                    let nacked = confirmation.is_nack();
                    let acked = confirmation.is_ack();
                    let msg = confirmation.take_message();

                    match msg {
                        Some(msg) => match msg.error() {
                            Some(e) => {
                                return Err(PublisherError::new(
                                    PublisherErrorKind::CommunicationError,
                                    Some(e.to_string()),
                                    Some(Box::new(e)),
                                ));
                            }
                            None => {
                                if nacked || !acked {
                                    return nacked_error;
                                }
                                return Ok(());
                            }
                        },
                        None => {
                            if nacked || !acked {
                                return nacked_error;
                            }
                            return Ok(());
                        }
                    }
                }
                Err(e) => {
                    return Err(PublisherError::new(
                        PublisherErrorKind::CommunicationError,
                        Some(e.to_string()),
                        Some(Box::new(e)),
                    ));
                }
            },
            Err(e) => {
                return Err(PublisherError::new(
                    PublisherErrorKind::CommunicationError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                ));
            }
        }
    }

    async fn publish(&mut self, payload: &[u8]) -> Result<(), PublisherError> {
        if self.connection.is_none() || self.channel.is_none() {
            let _ = self.connect().await?;
        }

        if let Some(ref chan) = self.channel {
            let props = BasicProperties::default().with_delivery_mode(2);
            let confirm = chan
                .basic_publish(
                    "",
                    &self.config.queue,
                    BasicPublishOptions {
                        mandatory: true,
                        immediate: false,
                    },
                    payload,
                    props,
                )
                .await;
            match self.handle_confirm(confirm).await {
                Ok(_) => return Ok(()),
                Err(e) => return Err(e),
            }
        }

        Err(PublisherError::new(
            PublisherErrorKind::CommunicationError,
            Some("Channel unavailable".to_string()),
            None,
        ))
    }

    async fn reset_members(&mut self) {
        if let Some(chan) = &self.channel {
            let _ = chan.close(200, "Close channel").await;
            self.channel = None;
        }
        if let Some(conn) = &self.connection {
            let _ = conn.close(200, "Close connection").await;
            self.connection = None;
        }
    }
}
