use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct PublisherError {
    kind: PublisherErrorKind,
    detail: Option<String>,
    source: Option<Box<dyn Error + Send + Sync>>,
}

#[derive(Debug, Clone)]
pub enum PublisherErrorKind {
    GenericError,
    ConnectionError,
    CommunicationError,
}

impl Error for PublisherError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.source {
            Some(s) => s.source(),
            None => None,
        }
    }
}

impl fmt::Display for PublisherError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let detail = match &self.detail {
            Some(d) => d.to_owned(),
            None => "Unknown".to_string(),
        };

        write!(f, "{}: {}", self.kind.as_str(), &detail)
    }
}

impl PublisherError {
    pub fn new(
        kind: PublisherErrorKind,
        detail: Option<String>,
        source: Option<Box<dyn Error + Send + Sync>>,
    ) -> PublisherError {
        PublisherError {
            kind,
            detail,
            source,
        }
    }

    pub fn kind(&self) -> PublisherErrorKind {
        self.kind.clone()
    }
}

impl From<PublisherErrorKind> for PublisherError {
    fn from(kind: PublisherErrorKind) -> Self {
        let detail = String::from(kind.as_str());
        PublisherError::new(kind, Some(detail), None)
    }
}

impl PublisherErrorKind {
    pub(crate) fn as_str(&self) -> &'static str {
        match *self {
            PublisherErrorKind::GenericError => "Failed to publish",
            PublisherErrorKind::ConnectionError => "Publisher connection failed",
            PublisherErrorKind::CommunicationError => "Communication Failed",
        }
    }
}
