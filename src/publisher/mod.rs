use core::fmt::Debug;

use async_trait::async_trait;

mod error;
mod rmq;

#[async_trait]
pub trait Publish: Debug {
    async fn connect(&mut self) -> Result<(), error::PublisherError>;
    async fn publish(&mut self, payload: &[u8]) -> Result<(), error::PublisherError>;
    async fn close(&mut self);
}

pub use error::*;
pub use rmq::*;
