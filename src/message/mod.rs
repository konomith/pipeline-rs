use core::fmt::Debug;

use crate::types::{ActionType, ResponseType};

mod default;
mod docker;
mod echo;
mod error;
mod response;

pub use echo::*;
pub use error::{MessageError, MessageErrorKind};

pub use default::*;
pub use docker::*;
pub use response::*;

pub trait Message<'a> {
    fn id(&self) -> &str {
        "<UNKNOWN>"
    }
    fn serialize(&self) -> Result<Vec<u8>, MessageError>;
    fn deserialize(raw: &'a [u8]) -> Result<Self, MessageError>
    where
        Self: Sized;
    fn action(&self) -> ActionType;
    fn set_id(&mut self, id: &str);
    fn response_metadata(&self) -> Option<&ResponseMetadata> {
        None
    }
    fn response_metadata_mut(&mut self) -> Option<&mut ResponseMetadata> {
        None
    }
}

pub trait Payload: Debug + Clone {
    type Value;

    fn payload(&self) -> Option<&Self::Value>;
}

pub trait ResponsePayload: Debug + Clone {
    type Value;

    fn payload(&self) -> &Self::Value;
}

pub trait MetadataResponser: Debug + Clone {
    fn method(&self) -> ResponseType;
    fn set_result(&mut self, _result: &ResultMetadata) {}
}
