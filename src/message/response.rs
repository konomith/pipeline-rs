use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json;

use crate::message::{Message, MessageError, MessageErrorKind, MetadataResponser, ResponsePayload};
use crate::types::{ActionType, ResponseType};
use crate::util::vars;

#[derive(Clone, Debug)]
pub struct ResultMetadata {
    pub success: bool,
    pub message: String,
    pub id: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct WebhookPayload {
    pub address: String,
    pub payload: String,
    pub headers: HashMap<String, String>,
    pub auth_required: bool,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct WebhookResponseMetadata {
    method: ResponseType,
    payload: WebhookPayload,
}

impl WebhookResponseMetadata {
    pub fn new(payload: WebhookPayload) -> Self {
        WebhookResponseMetadata {
            method: ResponseType::Webhook,
            payload,
        }
    }
}

impl ResponsePayload for WebhookResponseMetadata {
    type Value = WebhookPayload;

    fn payload(&self) -> &Self::Value {
        &self.payload
    }
}

impl MetadataResponser for WebhookResponseMetadata {
    fn method(&self) -> ResponseType {
        self.method.clone()
    }

    fn set_result(&mut self, result: &ResultMetadata) {
        self.payload
            .headers
            .insert(vars::WEBHOOK_HEADER_ID.to_owned(), result.id.to_owned());
        self.payload.headers.insert(
            vars::WEBHOOK_HEADER_STATUS.to_owned(),
            result.success.to_string(),
        );
        self.payload.headers.insert(
            vars::WEBHOOK_HEADER_RESPONSE.to_owned(),
            result.message.to_lowercase().to_owned(),
        );
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EmailPayload {
    pub email: String,
    pub payload: String,
    pub subject: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EmailResponseMetadata {
    method: ResponseType,
    payload: EmailPayload,
}

impl EmailResponseMetadata {
    pub fn new(payload: EmailPayload) -> Self {
        EmailResponseMetadata {
            method: ResponseType::Email,
            payload,
        }
    }
}

impl ResponsePayload for EmailResponseMetadata {
    type Value = EmailPayload;

    fn payload(&self) -> &Self::Value {
        &self.payload
    }
}

impl MetadataResponser for EmailResponseMetadata {
    fn method(&self) -> ResponseType {
        self.method.clone()
    }

    fn set_result(&mut self, result: &ResultMetadata) {
        let body = &mut self.payload.payload;
        let extra = format!(
            "\nid={}, status={}, message={}",
            result.id, result.success, result.message
        );
        body.push_str(&extra);
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ConsolePayload {
    pub header: String,
    pub result: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ConsoleResponseMetadata {
    method: ResponseType,
    payload: ConsolePayload,
}

impl ConsoleResponseMetadata {
    pub fn new(payload: ConsolePayload) -> Self {
        ConsoleResponseMetadata {
            method: ResponseType::Console,
            payload,
        }
    }
}

impl ResponsePayload for ConsoleResponseMetadata {
    type Value = ConsolePayload;

    fn payload(&self) -> &Self::Value {
        &self.payload
    }
}

impl MetadataResponser for ConsoleResponseMetadata {
    fn method(&self) -> ResponseType {
        self.method.clone()
    }

    fn set_result(&mut self, result: &ResultMetadata) {
        self.payload.result = format!(
            "id={}, status={}, message={}",
            result.id, result.success, result.message
        );
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TcpPayload {
    pub host: String,
    pub port: u16,
    pub result: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TcpResponseMetadata {
    method: ResponseType,
    payload: TcpPayload,
}

impl TcpResponseMetadata {
    pub fn new(payload: TcpPayload) -> Self {
        TcpResponseMetadata {
            method: ResponseType::Tcp,
            payload,
        }
    }
}

impl ResponsePayload for TcpResponseMetadata {
    type Value = TcpPayload;

    fn payload(&self) -> &Self::Value {
        &self.payload
    }
}

impl MetadataResponser for TcpResponseMetadata {
    fn method(&self) -> ResponseType {
        self.method.clone()
    }

    fn set_result(&mut self, result: &ResultMetadata) {
        self.payload.result = format!(
            "id={}, status={}, message={}\n",
            result.id, result.success, result.message
        );
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UdpPayload {
    pub host: String,
    pub port: u16,
    pub result: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UdpResponseMetadata {
    method: ResponseType,
    payload: UdpPayload,
}

impl UdpResponseMetadata {
    pub fn new(payload: UdpPayload) -> Self {
        UdpResponseMetadata {
            method: ResponseType::Udp,
            payload,
        }
    }
}

impl ResponsePayload for UdpResponseMetadata {
    type Value = UdpPayload;

    fn payload(&self) -> &Self::Value {
        &self.payload
    }
}

impl MetadataResponser for UdpResponseMetadata {
    fn method(&self) -> ResponseType {
        self.method.clone()
    }

    fn set_result(&mut self, result: &ResultMetadata) {
        self.payload.result = format!(
            "id={}, status={}, message={}\n",
            result.id, result.success, result.message
        );
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ResponseMetadata {
    WEBHOOK(WebhookResponseMetadata),
    EMAIL(EmailResponseMetadata),
    CONSOLE(ConsoleResponseMetadata),
    TCP(TcpResponseMetadata),
    UDP(UdpResponseMetadata),
}

impl MetadataResponser for ResponseMetadata {
    fn method(&self) -> ResponseType {
        match *self {
            Self::WEBHOOK(ref metadata) => metadata.method(),
            Self::CONSOLE(ref metadata) => metadata.method(),
            Self::EMAIL(ref metadata) => metadata.method(),
            Self::TCP(ref metadata) => metadata.method(),
            Self::UDP(ref metadata) => metadata.method(),
        }
    }

    fn set_result(&mut self, result: &ResultMetadata) {
        match *self {
            Self::WEBHOOK(ref mut metadata) => metadata.set_result(result),
            Self::CONSOLE(ref mut metadata) => metadata.set_result(result),
            Self::EMAIL(ref mut metadata) => metadata.set_result(result),
            Self::TCP(ref mut metadata) => metadata.set_result(result),
            Self::UDP(ref mut metadata) => metadata.set_result(result),
        }
    }
}

impl ResponseMetadata {
    pub fn method(&self) -> ResponseType {
        match *self {
            Self::WEBHOOK(ref metadata) => metadata.method(),
            Self::CONSOLE(ref metadata) => metadata.method(),
            Self::EMAIL(ref metadata) => metadata.method(),
            Self::TCP(ref metadata) => metadata.method(),
            Self::UDP(ref metadata) => metadata.method(),
        }
    }

    pub fn as_webhook(&self) -> Option<&WebhookResponseMetadata> {
        match *self {
            Self::WEBHOOK(ref metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_webhook_mut(&mut self) -> Option<&mut WebhookResponseMetadata> {
        match *self {
            Self::WEBHOOK(ref mut metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_email(&self) -> Option<&EmailResponseMetadata> {
        match *self {
            Self::EMAIL(ref metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_email_mut(&mut self) -> Option<&mut EmailResponseMetadata> {
        match *self {
            Self::EMAIL(ref mut metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_console(&self) -> Option<&ConsoleResponseMetadata> {
        match *self {
            Self::CONSOLE(ref metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_console_mut(&mut self) -> Option<&mut ConsoleResponseMetadata> {
        match *self {
            Self::CONSOLE(ref mut metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_tcp(&self) -> Option<&TcpResponseMetadata> {
        match *self {
            Self::TCP(ref metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_tcp_mut(&mut self) -> Option<&mut TcpResponseMetadata> {
        match *self {
            Self::TCP(ref mut metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_udp(&self) -> Option<&UdpResponseMetadata> {
        match *self {
            Self::UDP(ref metadata) => Some(metadata),
            _ => None,
        }
    }

    pub fn as_udp_mut(&mut self) -> Option<&mut UdpResponseMetadata> {
        match *self {
            Self::UDP(ref mut metadata) => Some(metadata),
            _ => None,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct ResponseMessageInner {
    action: ActionType,
    response_metadata: ResponseMetadata,
}

#[derive(Clone, Debug)]
pub struct ResponseMessage {
    inner: ResponseMessageInner,
}

impl ResponseMessage {
    pub fn new(metadata: ResponseMetadata) -> ResponseMessage {
        let inner = ResponseMessageInner {
            action: ActionType::Response,
            response_metadata: metadata,
        };

        ResponseMessage { inner }
    }
}

impl<'a> Message<'a> for ResponseMessage {
    fn serialize(&self) -> Result<Vec<u8>, MessageError> {
        match serde_json::to_vec(&self.inner) {
            Ok(result) => Ok(result),
            Err(e) => {
                let err = MessageError::new(
                    MessageErrorKind::SerializationError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    fn deserialize(raw: &'a [u8]) -> Result<Self, MessageError> {
        match serde_json::from_slice(raw) {
            Ok(result) => Ok(ResponseMessage { inner: result }),
            Err(e) => Err(MessageError::new(
                MessageErrorKind::DeserializationError,
                Some(e.to_string()),
                Some(Box::new(e)),
            )),
        }
    }

    fn action(&self) -> ActionType {
        self.inner.action.clone()
    }

    fn response_metadata(&self) -> Option<&ResponseMetadata> {
        Some(&self.inner.response_metadata)
    }

    fn response_metadata_mut(&mut self) -> Option<&mut ResponseMetadata> {
        Some(&mut self.inner.response_metadata)
    }

    fn set_id(&mut self, _id: &str) {}
}
