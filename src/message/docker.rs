use std::fmt;

use serde::{Deserialize, Serialize};
use serde_json;

use crate::message::{Message, MessageError, MessageErrorKind, Payload, ResponseMetadata};
use crate::types::ActionType;
use crate::util::generate_id;

#[derive(Clone, Serialize, Deserialize)]
pub struct DockerCredentials {
    username: Option<String>,
    password: Option<String>,
}

impl DockerCredentials {
    pub fn new() -> Self {
        DockerCredentials {
            username: None,
            password: None,
        }
    }

    pub fn required(&self) -> bool {
        self.username.is_some() && self.password.is_some()
    }

    pub fn new_with_required(username: &str, password: &str) -> Self {
        DockerCredentials {
            username: Some(username.to_owned()),
            password: Some(password.to_owned()),
        }
    }

    pub fn username(&self) -> &Option<String> {
        &self.username
    }

    pub fn password(&self) -> &Option<String> {
        &self.password
    }
}

impl fmt::Debug for DockerCredentials {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let username = match self.username {
            Some(ref u) => u.to_owned(),
            None => "null".to_string(),
        };

        let password = match self.password {
            Some(_) => "***********".to_string(),
            None => "null".to_string(),
        };

        f.debug_struct("DockerCredentials")
            .field("username", &username)
            .field("password", &password)
            .finish()
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DockerImageDetails {
    host: String,
    name: String,
    tag: String,
    credentials: DockerCredentials,
}

impl DockerImageDetails {
    pub fn new(
        host: &str,
        name: &str,
        tag: &str,
        username: Option<String>,
        password: Option<String>,
    ) -> DockerImageDetails {
        DockerImageDetails {
            host: host.to_string(),
            name: name.to_string(),
            tag: tag.to_string(),
            credentials: DockerCredentials { username, password },
        }
    }

    pub fn host(&self) -> &str {
        &self.host
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn tag(&self) -> &str {
        &self.tag
    }

    pub fn credentials(&self) -> &DockerCredentials {
        &self.credentials
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename = "payload")]
pub struct DockerImagePayload {
    source: DockerImageDetails,
    destination: DockerImageDetails,
}

impl DockerImagePayload {
    pub fn new(source: DockerImageDetails, destination: DockerImageDetails) -> Self {
        DockerImagePayload {
            source,
            destination,
        }
    }

    pub fn source(&self) -> &DockerImageDetails {
        &self.source
    }

    pub fn destination(&self) -> &DockerImageDetails {
        &self.destination
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct DockerImageMessageInner {
    id: String,
    action: ActionType,
    payload: DockerImagePayload,
    response_metadata: Option<ResponseMetadata>,
}

#[derive(Debug, Clone)]
pub struct DockerImageMessage {
    inner: DockerImageMessageInner,
}

impl Payload for DockerImageMessage {
    type Value = DockerImagePayload;

    fn payload(&self) -> Option<&Self::Value> {
        Some(&self.inner.payload)
    }
}

impl<'a> Message<'a> for DockerImageMessage {
    fn id(&self) -> &str {
        &self.inner.id
    }

    fn set_id(&mut self, id: &str) {
        self.inner.id = id.to_string();
    }

    fn serialize(&self) -> Result<Vec<u8>, MessageError> {
        match serde_json::to_vec(&self.inner) {
            Ok(result) => Ok(result),
            Err(e) => {
                let err = MessageError::new(
                    MessageErrorKind::SerializationError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    fn deserialize(raw: &'a [u8]) -> Result<Self, MessageError> {
        match serde_json::from_slice(raw) {
            Ok(result) => Ok(DockerImageMessage { inner: result }),
            Err(e) => Err(MessageError::new(
                MessageErrorKind::DeserializationError,
                Some(e.to_string()),
                Some(Box::new(e)),
            )),
        }
    }

    fn action(&self) -> ActionType {
        self.inner.action.clone()
    }

    fn response_metadata(&self) -> Option<&ResponseMetadata> {
        if let Some(ref rm) = self.inner.response_metadata {
            return Some(rm);
        }
        None
    }

    fn response_metadata_mut(&mut self) -> Option<&mut ResponseMetadata> {
        if let Some(ref mut rm) = self.inner.response_metadata {
            return Some(rm);
        }
        None
    }
}

impl DockerImageMessage {
    pub fn new(
        source: DockerImageDetails,
        destination: DockerImageDetails,
        response: Option<ResponseMetadata>,
    ) -> DockerImageMessage {
        DockerImageMessage {
            inner: DockerImageMessageInner {
                id: generate_id(),
                action: ActionType::DockerImage,
                payload: DockerImagePayload::new(source, destination),
                response_metadata: response,
            },
        }
    }

    pub fn source(&self) -> &DockerImageDetails {
        &self.inner.payload.source()
    }

    pub fn destination(&self) -> &DockerImageDetails {
        &self.inner.payload.destination()
    }
}
