use serde::{Deserialize, Serialize};
use serde_json;

use crate::message::{Message, MessageError, MessageErrorKind, ResponseMetadata};
use crate::types::ActionType;

#[derive(Clone, Debug, Serialize, Deserialize)]
struct BaseMessageInner {
    action: ActionType,
    response_metadata: Option<ResponseMetadata>,
}

#[derive(Clone, Debug)]
pub struct BaseMessage {
    inner: BaseMessageInner,
}

impl<'a> Message<'a> for BaseMessage {
    fn serialize(&self) -> Result<Vec<u8>, MessageError> {
        match serde_json::to_vec(&self.inner) {
            Ok(result) => Ok(result),
            Err(e) => {
                let err = MessageError::new(
                    MessageErrorKind::SerializationError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    fn deserialize(raw: &'a [u8]) -> Result<Self, MessageError> {
        match serde_json::from_slice(raw) {
            Ok(result) => Ok(BaseMessage { inner: result }),
            Err(e) => {
                if e.to_string().contains("missing field `action`") {
                    return Err(MessageError::new(
                        MessageErrorKind::DeserializationError,
                        Some("Deserialized message didn't contain `action`".to_string()),
                        None,
                    ));
                }

                Err(MessageError::new(
                    MessageErrorKind::DeserializationError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                ))
            }
        }
    }

    fn action(&self) -> ActionType {
        self.inner.action.clone()
    }

    fn response_metadata(&self) -> Option<&ResponseMetadata> {
        if let Some(ref rm) = self.inner.response_metadata {
            return Some(rm);
        }
        None
    }

    fn response_metadata_mut(&mut self) -> Option<&mut ResponseMetadata> {
        if let Some(ref mut rm) = self.inner.response_metadata {
            return Some(rm);
        }
        None
    }

    fn set_id(&mut self, _id: &str) {}
}
