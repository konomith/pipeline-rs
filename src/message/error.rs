use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct MessageError {
    kind: MessageErrorKind,
    detail: Option<String>,
    source: Option<Box<dyn Error + Send + Sync>>,
}

#[derive(Debug, Clone)]
pub enum MessageErrorKind {
    SerializationError,
    DeserializationError,
}

impl Error for MessageError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.source {
            Some(s) => s.source(),
            None => None,
        }
    }
}

impl fmt::Display for MessageError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let detail = match &self.detail {
            Some(d) => d.to_owned(),
            None => "Unknown".to_string(),
        };

        write!(f, "{}: {}", self.kind.as_str(), &detail)
    }
}

impl MessageError {
    pub fn new(
        kind: MessageErrorKind,
        detail: Option<String>,
        source: Option<Box<dyn Error + Send + Sync>>,
    ) -> MessageError {
        MessageError {
            kind,
            detail,
            source,
        }
    }

    pub fn kind(&self) -> MessageErrorKind {
        self.kind.clone()
    }
}

impl From<MessageErrorKind> for MessageError {
    fn from(kind: MessageErrorKind) -> Self {
        let detail = String::from(kind.as_str());
        MessageError::new(kind, Some(detail), None)
    }
}

impl MessageErrorKind {
    pub(crate) fn as_str(&self) -> &'static str {
        match *self {
            MessageErrorKind::SerializationError => "Failed to serialize the message",
            MessageErrorKind::DeserializationError => "Failed to deserialize the message",
        }
    }
}
