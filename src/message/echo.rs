use serde::{Deserialize, Serialize};
use serde_json;

use crate::message::{Message, MessageError, MessageErrorKind, Payload, ResponseMetadata};
use crate::types::ActionType;
use crate::util::generate_id;

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename = "payload")]
pub struct EchoMessagePayload {
    pub message: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct EchoMessageInner {
    id: String,
    action: ActionType,
    payload: EchoMessagePayload,
    response_metadata: Option<ResponseMetadata>,
}

#[derive(Debug, Clone)]
pub struct EchoMessage {
    inner: EchoMessageInner,
}

impl Payload for EchoMessage {
    type Value = EchoMessagePayload;
    fn payload(&self) -> Option<&Self::Value> {
        Some(&self.inner.payload)
    }
}

impl<'a> Message<'a> for EchoMessage {
    fn id(&self) -> &str {
        &self.inner.id
    }

    fn set_id(&mut self, id: &str) {
        self.inner.id = id.to_string();
    }

    fn serialize(&self) -> Result<Vec<u8>, MessageError> {
        match serde_json::to_vec(&self.inner) {
            Ok(result) => Ok(result),
            Err(e) => {
                let err = MessageError::new(
                    MessageErrorKind::SerializationError,
                    Some(e.to_string()),
                    Some(Box::new(e)),
                );
                Err(err)
            }
        }
    }

    fn deserialize(raw: &'a [u8]) -> Result<Self, MessageError> {
        match serde_json::from_slice(raw) {
            Ok(result) => Ok(EchoMessage { inner: result }),
            Err(e) => Err(MessageError::new(
                MessageErrorKind::DeserializationError,
                Some(e.to_string()),
                Some(Box::new(e)),
            )),
        }
    }

    fn action(&self) -> ActionType {
        self.inner.action.clone()
    }

    fn response_metadata(&self) -> Option<&ResponseMetadata> {
        if let Some(ref rm) = self.inner.response_metadata {
            return Some(rm);
        }
        None
    }

    fn response_metadata_mut(&mut self) -> Option<&mut ResponseMetadata> {
        if let Some(ref mut rm) = self.inner.response_metadata {
            return Some(rm);
        }
        None
    }
}

impl EchoMessage {
    pub fn new(msg: &str) -> Self {
        EchoMessage {
            inner: EchoMessageInner {
                id: generate_id(),
                action: ActionType::Echo,
                payload: EchoMessagePayload {
                    message: msg.to_string(),
                },
                response_metadata: None,
            },
        }
    }

    pub fn with_response_metadata(mut self, metadata: ResponseMetadata) -> Self {
        self.inner.response_metadata = Some(metadata);
        self
    }

    pub fn message(&self) -> &str {
        &self.inner.payload.message
    }
}
