use core::hash::Hash;
use serde::{Deserialize, Serialize};

#[derive(Debug, Hash, PartialEq, Eq, Serialize, Deserialize, Clone)]
#[serde(tag = "type", content = "name")]
pub enum ActionType {
    Echo,
    DockerImage,
    Response,
    CustomAction(String),
}

#[derive(Debug, Hash, PartialEq, Eq, Serialize, Deserialize, Clone)]
#[serde(tag = "type", content = "name")]
#[serde(rename_all = "lowercase")]
pub enum ResponseType {
    Tcp,
    Udp,
    Email,
    Webhook,
    Console,
    Custom(String),
}
