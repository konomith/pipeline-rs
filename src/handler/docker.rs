use std::collections::HashSet;
use std::default::Default;

use async_trait::async_trait;
use bollard::{
    auth::DockerCredentials as DockerApiCredentials,
    errors::Error as DockerError,
    image::{CreateImageOptions, PushImageOptions, TagImageOptions},
    Docker,
};
use futures_lite::StreamExt;
use log;

use crate::config::Config;
use crate::handler::{error::HandlerError, HandleResult, MessageHandler};
use crate::message::{DockerImageDetails, DockerImageMessage, ResponseMessage, ResultMetadata};
use crate::prelude::*;

lazy_static! {
    static ref CONNECTION_METHODS: HashSet<&'static str> =
        ["local", "ssl", "http"].iter().cloned().collect();
}

const DEFAULT_HOST: &str = "0.0.0.0:5000";

#[derive(Debug)]
pub struct DockerImageHandler {
    client: Docker,
}

impl DockerImageHandler {
    pub fn new(config: &Config) -> Result<Self, HandlerError> {
        let client = Self::init_client(config)?;
        Ok(DockerImageHandler { client })
    }

    fn init_client(config: &Config) -> Result<Docker, DockerError> {
        let connect_method = match config.get_str("docker", "method") {
            Some(method) => {
                let method = if CONNECTION_METHODS.contains(method) {
                    method
                } else {
                    "local"
                };
                method
            }
            None => "local",
        };

        let client = match connect_method {
            "local" => Docker::connect_with_local_defaults()?,
            "ssl" => Docker::connect_with_ssl_defaults()?,
            "http" => Docker::connect_with_http_defaults()?,
            _ => Docker::connect_with_local_defaults()?,
        };
        Ok(client)
    }

    async fn pull(
        &self,
        message: &DockerImageMessage,
        _config: &Config,
    ) -> Result<(), DockerError> {
        let source = message.source();
        let opts = CreateImageOptions {
            from_src: source.host(),
            from_image: &format!("{}/{}", source.host(), source.name()),
            tag: source.tag(),
            ..Default::default()
        };
        let creds = if source.credentials().required() {
            Some(DockerApiCredentials {
                username: source.credentials().username().clone(),
                password: source.credentials().password().clone(),
                ..Default::default()
            })
        } else {
            None
        };

        self.client
            .create_image(Some(opts), None, creds)
            .try_for_each::<_, DockerError>(|line| match line {
                Ok(line) => {
                    log::info!("DockerImageHandler: pull: {:?}", line);
                    Ok(())
                }
                Err(e) => Err(e),
            })
            .await?;
        Ok(())
    }

    async fn push(&self, message: &DockerImageMessage, config: &Config) -> Result<(), DockerError> {
        let host = if config
            .get_bool("executor", "upstream_sentinel")
            .unwrap_or(false)
        {
            message.destination().host()
        } else {
            config.get_str("docker", "host").unwrap_or(DEFAULT_HOST)
        };
        let name = message.destination().name();
        let tag = message.destination().tag();

        let tag_options = TagImageOptions {
            repo: &format!("{}/{}", host, name)[..],
            tag,
        };

        let source_name = message.source().name();
        let source_tag = message.source().tag();
        let source_host = message.source().host();

        match self
            .client
            .tag_image(
                &format!("{}/{}:{}", source_host, source_name, source_tag),
                Some(tag_options),
            )
            .await
        {
            Ok(_) => {
                log::info!(
                    "Successfully tagged image: {}/{}:{} to {}/{}:{}",
                    source_host,
                    source_name,
                    source_tag,
                    host,
                    name,
                    tag
                );
            }
            Err(e) => {
                log::error!("Failed to tag image: {:?}", e);
                return Err(e);
            }
        }
        let push_options = PushImageOptions { tag };

        let creds = if config
            .get_bool("executor", "upstream_sentinel")
            .unwrap_or(false)
        {
            if message.destination().credentials().required() {
                Some(DockerApiCredentials {
                    username: message.destination().credentials().username().clone(),
                    password: message.destination().credentials().password().clone(),
                    ..Default::default()
                })
            } else {
                None
            }
        } else {
            if config.get_bool("docker", "auth_required").unwrap_or(false) {
                Some(DockerApiCredentials {
                    username: Some(
                        config
                            .get_str("docker", "username")
                            .unwrap_or("")
                            .to_owned(),
                    ),
                    password: Some(
                        config
                            .get_str("docker", "password")
                            .unwrap_or("")
                            .to_owned(),
                    ),
                    ..Default::default()
                })
            } else {
                None
            }
        };

        self.client
            .push_image(&format!("{}/{}", host, name), Some(push_options), creds)
            .try_for_each(|line| match line {
                Ok(line) => {
                    log::info!("DockerImageHandler: push: {:?}", line);
                    Ok(())
                }
                Err(e) => Err(e),
            })
            .await?;
        Ok(())
    }

    fn generate_source(&self, message: &DockerImageMessage, config: &Config) -> DockerImageDetails {
        DockerImageDetails::new(
            config.get_str("docker", "host").unwrap_or(DEFAULT_HOST),
            message.destination().name(),
            message.destination().tag(),
            config.get_str("docker", "username").map(String::from),
            config.get_str("docker", "password").map(String::from),
        )
    }

    fn generate_forward(
        &self,
        message: &DockerImageMessage,
        config: &Config,
    ) -> DockerImageMessage {
        let source = self.generate_source(message, config);
        let destination = message.destination().clone();
        let response_metadata = message.response_metadata().cloned();

        let mut forward = DockerImageMessage::new(source, destination, response_metadata);
        forward.set_id(message.id());
        forward
    }

    fn generate_reply(
        &self,
        message: &DockerImageMessage,
        success: bool,
    ) -> Option<ResponseMessage> {
        let id = message.id().to_owned();

        let detail = if success {
            "success"
        } else {
            "docker image transfer failed"
        };

        if let Some(mut response) = message.response_metadata().cloned() {
            let res_metadata = ResultMetadata {
                id,
                success,
                message: detail.to_owned(),
            };
            response.set_result(&res_metadata);

            return Some(ResponseMessage::new(response));
        }

        None
    }

    fn generate_response(
        &self,
        success: bool,
        message: &DockerImageMessage,
        config: &Config,
    ) -> HandleResult {
        let mut res = HandleResult {
            success,
            forward: None,
            reply: None,
        };

        if config
            .get_bool("executor", "upstream_sentinel")
            .unwrap_or(false)
            || !success
        {
            let reply = match self.generate_reply(message, success) {
                Some(response) => response.serialize().ok(),
                None => None,
            };
            res.reply = reply;
        } else {
            let forward = self.generate_forward(message, config);
            res.forward = forward.serialize().ok();
        }

        res
    }
}

#[async_trait]
impl MessageHandler for DockerImageHandler {
    async fn handle(&self, raw: &[u8], config: &Config) -> HandleResult {
        match DockerImageMessage::deserialize(raw) {
            Ok(message) => {
                log::info!("Received DockerImageHandler task with id={}", message.id());
                let mut success = true;
                match self.pull(&message, config).await {
                    Ok(_) => {}
                    Err(e) => {
                        success = false;
                        log::warn!("DockerImageHandler: handle: pull: {:?}", e);
                    }
                }

                if success {
                    match self.push(&message, config).await {
                        Ok(_) => {}
                        Err(e) => {
                            success = false;
                            log::warn!("DockerImageHandler: handle: push: {:?}", e);
                        }
                    }
                }

                log::info!("Completed DockerImageHandler task with id={}", message.id());
                self.generate_response(success, &message, config)
            }
            Err(e) => {
                log::warn!("DockerImageHandler: failed to parse message: {:?}", e);
                return HandleResult {
                    success: false,
                    forward: None,
                    reply: None,
                };
            }
        }
    }
}
