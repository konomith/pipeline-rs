use std::error::Error;
use std::fmt;

use bollard::errors::Error as DockerError;

#[derive(Debug)]
pub struct HandlerError {
    kind: HandlerErrorKind,
    detail: Option<String>,
    source: Option<Box<dyn Error + Send + Sync>>,
}

#[derive(Debug, Clone)]
pub enum HandlerErrorKind {
    DockerImageError,
}

impl Error for HandlerError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.source {
            Some(s) => s.source(),
            None => None,
        }
    }
}

impl fmt::Display for HandlerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let detail = match &self.detail {
            Some(d) => d.to_owned(),
            None => "Unknown".to_string(),
        };

        write!(f, "{}: {}", self.kind.as_str(), &detail)
    }
}

impl HandlerError {
    pub fn new(
        kind: HandlerErrorKind,
        detail: Option<String>,
        source: Option<Box<dyn Error + Send + Sync>>,
    ) -> HandlerError {
        HandlerError {
            kind,
            detail,
            source,
        }
    }

    pub fn kind(&self) -> HandlerErrorKind {
        self.kind.clone()
    }
}

impl From<HandlerErrorKind> for HandlerError {
    fn from(kind: HandlerErrorKind) -> Self {
        let detail = String::from(kind.as_str());
        HandlerError::new(kind, Some(detail), None)
    }
}

impl From<DockerError> for HandlerError {
    fn from(e: DockerError) -> Self {
        let detail = e.to_string();
        HandlerError::new(
            HandlerErrorKind::DockerImageError,
            Some(detail),
            Some(Box::new(e)),
        )
    }
}

impl HandlerErrorKind {
    pub(crate) fn as_str(&self) -> &'static str {
        match *self {
            HandlerErrorKind::DockerImageError => "Docker action error",
        }
    }
}
