#[derive(Debug, Clone)]
pub struct HandleResult {
    pub success: bool,
    pub forward: Option<Vec<u8>>,
    pub reply: Option<Vec<u8>>,
}
