use core::fmt::Debug;

use async_trait::async_trait;

use crate::config::Config;

mod docker;
mod echo;
pub mod error;
mod response;
mod result;

pub use docker::*;
pub use echo::*;
pub use response::*;
pub use result::*;

#[async_trait]
pub trait MessageHandler: Send + Sync + Debug {
    async fn handle(&self, raw: &[u8], config: &Config) -> HandleResult;
}
