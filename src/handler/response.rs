use std::time::{Duration, Instant};

use async_trait::async_trait;

use exporter::client::email::DefaultEmailClient;
use exporter::client::protocol::DefaultProtocolClient;
use log;
use tokio::sync::RwLock;
use tokio::time::sleep;

use crate::config::Config;
use crate::handler::{HandleResult, MessageHandler};
use crate::message::{EmailPayload, ResponseMessage, TcpPayload};
use crate::prelude::*;
use crate::types::ResponseType;

#[derive(Debug)]
struct ResponseExporterConnection {
    exporter_address: Option<String>,
    protocol: Option<DefaultProtocolClient>,
    email: Option<DefaultEmailClient>,
}

impl ResponseExporterConnection {
    fn new(config: &Config) -> Self {
        let exporter_address = config.get_str("response_handler", "exporter_address");
        ResponseExporterConnection {
            exporter_address: exporter_address.map(String::from),
            protocol: None,
            email: None,
        }
    }

    fn exporter_enabled(&self) -> bool {
        self.exporter_address.is_some()
    }

    async fn protocol(&mut self) -> Option<&DefaultProtocolClient> {
        if !self.exporter_enabled() {
            return None;
        }

        if self.protocol.is_some() && !self.protocol.as_ref().unwrap().broken() {
            return self.protocol.as_ref();
        }

        match DefaultProtocolClient::connect(self.exporter_address.as_ref().unwrap()).await {
            Ok(client) => {
                self.protocol = Some(client);
                self.protocol.as_ref()
            }
            Err(e) => {
                log::warn!(
                    "ProtocolClient: Unable to connect to exporter server: {}",
                    e
                );
                None
            }
        }
    }

    async fn email(&mut self) -> Option<&DefaultEmailClient> {
        if !self.exporter_enabled() {
            return None;
        }

        if self.email.is_some() && !self.email.as_ref().unwrap().broken() {
            return self.email.as_ref();
        }

        match DefaultEmailClient::connect(self.exporter_address.as_ref().unwrap()).await {
            Ok(client) => {
                self.email = Some(client);
                self.email.as_ref()
            }
            Err(e) => {
                log::warn!("EmailClient: Unable to connect to exporter server: {}", e);
                None
            }
        }
    }
}

async fn check_protocol_output(
    packet_id: u64,
    client: &DefaultProtocolClient,
    timeout: Duration,
) -> Option<String> {
    let now = Instant::now();
    while now.elapsed() < timeout {
        match client.check_output(packet_id).await {
            Some(result) => {
                return Some(result.clone());
            }
            None => sleep(Duration::from_secs(1)).await,
        };
    }
    None
}

async fn check_email_output(
    packet_id: u64,
    client: &DefaultEmailClient,
    timeout: Duration,
) -> Option<String> {
    let now = Instant::now();
    while now.elapsed() < timeout {
        match client.check_output(packet_id).await {
            Some(result) => {
                return Some(result.clone());
            }
            None => sleep(Duration::from_secs(1)).await,
        };
    }
    None
}

#[derive(Debug)]
pub struct ResponseHandler {
    connections: RwLock<ResponseExporterConnection>,
}

impl ResponseHandler {
    pub fn new(config: &Config) -> Self {
        Self {
            connections: RwLock::new(ResponseExporterConnection::new(config)),
        }
    }

    async fn handle_tcp(&self, payload: &TcpPayload, res: &mut HandleResult) {
        let mut conns = self.connections.write().await;
        if conns.exporter_enabled() {
            match conns.protocol().await {
                Some(client) => {
                    match client
                        .send_tcp(&payload.host, payload.port, &payload.result)
                        .await
                    {
                        Ok(packet_id) => {
                            log::info!("Response sent to Exporter Server: packet_id={}", packet_id);
                            match check_protocol_output(packet_id, client, Duration::from_secs(60))
                                .await
                            {
                                Some(result) => {
                                    log::info!(
                                        "Exporter Server result for TCP packet_id={}: {}",
                                        packet_id,
                                        result
                                    );
                                }
                                None => {
                                    log::info!("Timeout while waiting for Exporter Server result for TCP packet_id={}", packet_id);
                                }
                            }
                        }
                        Err(e) => {
                            res.success = false;
                            log::warn!("Failed to send response to Exporter Server: {}", e);
                        }
                    }
                }
                None => {
                    res.success = false;
                    log::warn!("Unable to connect to Exporter Server");
                }
            }
        } else {
            res.success = false;
            log::warn!("Response handler hasn't been configured to use Exporter Server");
        }
    }

    async fn handle_email(&self, payload: &EmailPayload, res: &mut HandleResult) {
        let mut conns = self.connections.write().await;
        if conns.exporter_enabled() {
            match conns.email().await {
                Some(client) => {
                    match client
                        .send::<&str>(&payload.email, &payload.subject, &payload.payload, None)
                        .await
                    {
                        Ok(packet_id) => {
                            log::info!("Response sent to Exporter Server: packet_id={}", packet_id);
                            match check_email_output(packet_id, client, Duration::from_secs(60))
                                .await
                            {
                                Some(result) => {
                                    log::info!(
                                        "Exporter Server result for EMAIL packet_id={}: {}",
                                        packet_id,
                                        result
                                    );
                                }
                                None => {
                                    log::info!("Timeout while waiting for Exporter Server result for EMAIL packet_id={}", packet_id);
                                }
                            }
                        }
                        Err(e) => {
                            res.success = false;
                            log::warn!("Failed to send response to Exporter Server: {}", e);
                        }
                    }
                }
                None => {
                    res.success = false;
                    log::warn!("Unable to connect to Exporter Server");
                }
            }
        } else {
            res.success = false;
            log::warn!("Response handler hasn't been configured to use Exporter Server");
        }
    }
}

#[async_trait]
impl MessageHandler for ResponseHandler {
    async fn handle(&self, raw: &[u8], _config: &Config) -> HandleResult {
        let mut res = HandleResult {
            success: true,
            forward: None,
            reply: None,
        };

        match ResponseMessage::deserialize(raw) {
            Ok(message) => {
                let metadata = message.response_metadata();
                match metadata {
                    Some(metadata) => match metadata.method() {
                        ResponseType::Console => {
                            let payload = metadata.as_console().unwrap().payload();
                            println!("[ResponseHandler|Console: {:?}", &payload);
                        }
                        ResponseType::Tcp => {
                            let payload = metadata.as_tcp().unwrap().payload();
                            self.handle_tcp(payload, &mut res).await;
                        }
                        ResponseType::Email => {
                            let payload = metadata.as_email().unwrap().payload();
                            self.handle_email(payload, &mut res).await;
                        }
                        _ => {
                            println!("[ResponseHandler|NoMethodYet]");
                        }
                    },
                    None => {}
                }
            }
            Err(_) => res.success = false,
        }
        res
    }
}
