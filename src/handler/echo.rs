use std::str::from_utf8;

use async_trait::async_trait;

use crate::config::Config;
use crate::handler::{HandleResult, MessageHandler};
use crate::message::{EchoMessage, ResponseMessage, ResultMetadata};
use crate::prelude::*;

#[derive(Debug)]
pub struct EchoHandler;

#[async_trait]
impl MessageHandler for EchoHandler {
    async fn handle(&self, raw: &[u8], _config: &Config) -> HandleResult {
        log::debug!(
            "Got raw message: {}",
            from_utf8(raw).unwrap_or("[NOT UTF8]")
        );
        let mut res = HandleResult {
            success: true,
            forward: None,
            reply: None,
        };

        match EchoMessage::deserialize(raw) {
            Ok(mut message) => {
                let msg = message.message();
                let id = message.id().to_owned();
                println!("EchoHandler: [{}] {}", id, msg);

                if let Some(response) = message.response_metadata_mut() {
                    let res_metadata = ResultMetadata {
                        id,
                        success: true,
                        message: "success".to_owned(),
                    };
                    response.set_result(&res_metadata);

                    let cloned = response.clone();
                    let response_message = ResponseMessage::new(cloned);
                    res.reply = response_message.serialize().ok();

                    log::debug!(
                        "Reply will be: {}",
                        from_utf8(&res.reply.as_ref().unwrap()).unwrap()
                    );
                }
                res.forward = message.serialize().ok();
                log::debug!(
                    "Forward will be: {}",
                    from_utf8(&res.reply.as_ref().unwrap()).unwrap()
                );
            }
            Err(e) => {
                println!("error {}", e);
                res.success = false;
            }
        }

        res
    }
}
