use lapin::Channel;
use uuid;

const PROTOCOL: &str = "amqp://";
const PROTOCOL_SECURE: &str = "amqps://";

#[derive(Debug, Clone)]
pub struct ConnectionConfigBuilder {
    host: String,
    vhost: String,
    queue: String,
    auth_required: bool,
    username: String,
    password: String,
    secure: bool,
}

impl Default for ConnectionConfigBuilder {
    fn default() -> Self {
        ConnectionConfigBuilder {
            host: "localhost:5672".to_string(),
            queue: "rmq-consumer-rs-queue".to_string(),
            auth_required: false,
            vhost: "%2f".to_string(),
            username: "".to_string(),
            password: "".to_string(),
            secure: false,
        }
    }
}

impl ConnectionConfigBuilder {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn host(mut self, host: &str) -> Self {
        self.host = host.to_owned();
        self
    }

    pub fn vhost(mut self, vhost: &str) -> Self {
        self.vhost = vhost.to_owned();
        self
    }

    pub fn queue(mut self, queue: &str) -> Self {
        self.queue = queue.to_owned();
        self
    }

    pub fn username(mut self, username: &str) -> Self {
        self.username = username.to_owned();
        self
    }

    pub fn password(mut self, password: &str) -> Self {
        self.password = password.to_owned();
        self
    }

    pub fn auth_required(mut self, auth_required: bool) -> Self {
        self.auth_required = auth_required;
        self
    }

    pub fn secure(mut self, secure: bool) -> Self {
        self.secure = secure;
        self
    }

    pub fn build(&self) -> ConnectionConfig {
        ConnectionConfig {
            host: self.host.clone(),
            vhost: self.vhost.clone(),
            queue: self.queue.clone(),
            auth_required: self.auth_required,
            username: self.username.clone(),
            password: self.password.clone(),
            secure: self.secure,
        }
    }
}

#[derive(Debug, Clone)]
pub struct ConnectionConfig {
    pub(crate) host: String,
    pub(crate) vhost: String,
    pub(crate) queue: String,
    pub(crate) auth_required: bool,
    pub(crate) username: String,
    pub(crate) password: String,
    pub(crate) secure: bool,
}

impl ConnectionConfig {
    pub fn builder() -> ConnectionConfigBuilder {
        ConnectionConfigBuilder::new()
    }

    pub fn generate_uri(&self) -> String {
        let mut builder = String::new();

        // protocol
        if self.secure {
            builder.push_str(PROTOCOL_SECURE);
        } else {
            builder.push_str(PROTOCOL);
        }

        // auth
        if self.auth_required {
            builder.push_str(&format!("{}:{}@", self.username, self.password));
        }

        // host
        builder.push_str(&self.host);
        // vhost
        builder.push_str(&format!("/{}", self.vhost));
        builder
    }

    pub fn with_host(host: &str) -> Self {
        ConnectionConfig {
            host: host.to_string(),
            ..Self::default()
        }
    }
}

impl Default for ConnectionConfig {
    fn default() -> Self {
        ConnectionConfig {
            host: "localhost:5672".to_string(),
            queue: "rmq-consumer-rs-queue".to_string(),
            auth_required: false,
            vhost: "%2f".to_string(),
            username: "".to_string(),
            password: "".to_string(),
            secure: false,
        }
    }
}

pub fn generate_ctag(chan: &Channel) -> String {
    let cid = chan.id();
    let uid = uuid::Uuid::new_v4();
    let repr = uid.as_simple().to_string();
    let ctag = format!("ctag-{}-{}", cid, repr);
    ctag
}
