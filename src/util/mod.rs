use uuid;

pub mod rmq;
pub mod vars;

pub fn generate_id() -> String {
    uuid::Uuid::new_v4().to_string()
}
