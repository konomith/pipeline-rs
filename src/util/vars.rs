pub static WEBHOOK_HEADER_RESPONSE: &str = "x-pipeline-response";
pub static WEBHOOK_HEADER_STATUS: &str = "x-pipeline-status";
pub static WEBHOOK_HEADER_ID: &str = "x-pipeline-message-id";
